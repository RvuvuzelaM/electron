// To build we have to use Electron Packager
// https://www.christianengvall.se/electron-packager-tutorial/

const electron = require('electron');
const url = require('url');
const path = require('path');

const {app, BrowserWindow, Menu, ipcMain} = electron;

// SET ENV
process.env.NODE_ENV = 'production';

let mainWindow;
let addWindow;






// Listen for app to be ready
app.on('ready', function(){

  // Create new window
  mainWindow = new BrowserWindow({});

  mainWindow.setResizable(false);

  // Load html into window
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'mainwindow.html'),
    protocol: 'file:',
    slashes: true
  }));

  // Quit app when closed
  mainWindow.on('close', function(){
    app.quit()
  });

  // Build menu from template
  const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);

  //Insert menu
  Menu.setApplicationMenu(mainMenu);
});





// Handle add window
function createAddWindow(){
  while(addWindow == null){
    // Create new window
    addWindow = new BrowserWindow({
      parent: mainWindow,
      width: 300,
      height: 200,
      title: 'Add Shoping List Item'
    });

    addWindow.setResizable(false);

    // Load html into window
    addWindow.loadURL(url.format({
      pathname: path.join(__dirname, 'addwindow.html'),
      protocol: 'file:',
      slashes: true
    }));

    // Garbage Collection handle
    addWindow.on('close', function(){
        addWindow = null;
    });
  }  
}

// Catch item:add
  ipcMain.on('item:add', function(e, item){
    mainWindow.webContents.send('item:add', item);
    addWindow.close();
});





// Create menu template
const mainMenuTemplate = [
  {
    label: 'File',
    submenu:[
      {
        label: 'Add Item',
        click(){
          createAddWindow();
        }
      },
      {
        label: 'Clear Items',
        click(){
          mainWindow.webContents.send('item:clear');
        }
      },
      {
        label: 'Quit',
        // Shows on what platform we are working(for mac it is darwin)
        // node process.platform to check
        accelerator: process.platform == 'darwin' ? 'Command+Q' : 
        'Ctrl+Q',
        click(){
          app.quit();
        }
      }
    ]
  }
];






// if we are on mac, add empty object to menu to make app work properly
if(process.platform == 'darwin'){
  mainMenuTemplate.unshift({});
}

// Add developer tools item if not in prod
if(process.env.NODE_ENV !== 'production'){
  mainMenuTemplate.push({
    label: 'Developer Tools ',
    submenu:[
      {
        label: 'Toogle Devtools',
        accelerator: process.platform == 'darwin' ? 'Command+I' : 
        'Ctrl+I',
        click(item, focusedWindow){
          focusedWindow.toggleDevTools();
        }
      },
      {
        role: 'reload'
      }
    ]
  });
}